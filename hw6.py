""" Homework 6
-- Due Sunday, Mar. 1st at 23:59
-- Always write the final code yourself
-- Cite any websites you referenced
-- Use the PEP-8 checker for full style points:
https://pypi.python.org/pypi/pep8
"""


def phone_numbers(number_file, n):
    ''' Open a file and return the first n phone numbers in the text.
    If there are less then n phone numbers in the file, return as many
    as you can.
    If the file does not exist, raise an Exception with the message
    "No such file"

    A phone number has a 3-digit zip code, 3-digit prefix, 4-digit trunk,
    and optional multi-digit extension. The separators may be spaces,
    dashes, or periods, but should be consistent. The zip code may be
    enclosed in parenthesis. There is one exception to the separator
    consistency rule: the separator between the trunk and the extension
    may be the character "x", to indicate "extension". For instance,
    all of the following are valid: 800-555-1212, (800) 555 1212,
    800.555.1212.1234, (800)-555-1212x1234.
    '''
    pass


def sorted_dates(date_file):
    '''Find all the dates in the given file and output a list of the dates
       as strings of the form 'mm/dd/yyyy'.

       The list should be sorted in ascending chronological order.

       dates may be in the form: mm/dd/yyyy|mm/dd/yy
       in the latter case yy translates as yy -> 20yy

       If the file does not exist, raise an Exception with the message
       "No such file"
    '''
    pass


class MerriamWebster():
    ''' Write a class based on the accompanying "words.txt" file,
    which contains 235886 English words, one per line.

    An instance of this class should have one attribute, called "words,"
    which should contain a list of all words in "words.txt" of length > 2
    that start with a lowercase letter.

    This class should support the generator protocol, so we should be
    able to iterate over an instance and easily generate words from
    the self.words sequence, one at a time, as per the test file

    You should also implement the following two methods:

    -- substring(x, y, z ...): This method takes an arbitrary number
    of characters as input, computes all possible orderings of these
    characters, and returns a generator over all words in self.words
    that contain (as a substring) one of these orderings.

    For instance, substring('t', 'i', 'e') should generate all words in which
    any of the substrings "tie", "tei", "ite", "iet", "eti", or "eit" appears.

    For full style points, do this without any for loops! You might find it
    helpful to use Python's builtin "any" function:
    https://docs.python.org/3.4/library/functions.html#any

    -- trie(): Create and return a trie data structure to represent
    the contents of self.words. A trie is a special tree structure for
    efficient manipulation of dictionaries (not in the Python sense --
    but you should use a dictionary to implement it!). The basic idea
    is to share the storage of common prefixes.
    See here: http://en.wikipedia.org/wiki/Trie

    Your implementation should use nested dictionaries. Every word should
    end with the symbol "_". As an example, a trie containing the words
    "ball" and "ballet" would be as follows:
    d = {'b': {'a': {'l': {'l': {'_': None,
                                 'e': {'t': {'_': None}}}}}}}

    see the test file for examples
    '''
    WORDS = 'words.txt'

    def __init__(self):
        pass

    def __iter__(self):
        pass

    def substring(self, *args):
        pass

    def trie(self):
        pass


def sorted_files(directory):
    ''' Walk the specified directory and generate a list of all files found,
    in increasing order of their size in bytes. The filenames should be
    relative to the root directory, e.g. "directory/.../file.txt",
    rather than "file.txt".
    '''
    pass


def main():
    pass


if __name__ == "__main__":
    main()
