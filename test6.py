import unittest
import os
import itertools
from hw6 import *


class TestHomework6(unittest.TestCase):

    def setUp(self):
        pass

    # phone numbers
    def test_phone_numbers(self):
        PHONE_FILE = 'phone_test.txt'
        NUMS = ['234 523 4602', '(234)-456-7594',
                '800-555-1212', '(832) 621 8274',
                '098.765.5433.2341', '(999)-888-7777x66',
                '(098).765.4321']
        two_nums = phone_numbers(PHONE_FILE, 2)
        all_nums = phone_numbers(PHONE_FILE, 7)
        self.assertEqual(two_nums, NUMS[:2])
        self.assertEqual(all_nums, NUMS)
        with self.assertRaises(Exception, msg='No such file'):
            phone_numbers('some_nonsense.fake', 1)

    # sorted dates
    def test_sorted_dates(self):
        DATE_FILE = 'date_test.txt'
        DATES = ['01/23/1993', '02/18/2015', '12/14/2056']
        self.assertEqual(sorted_dates(DATE_FILE), DATES)
        with self.assertRaises(Exception, msg='No such file'):
            phone_numbers('some_nonsense.fake', 1)

    # Merriam Webster
    def test_MerriamWebster(self):
        mw = MerriamWebster()
        first_5 = list(itertools.islice(mw, 5))
        forty_thou = next(itertools.islice(mw, 40000, 40001))
        self.assertEqual(first_5, ['aal', 'aalii', 'aam',
                                   'aardvark', 'aardwolf'])
        self.assertEqual(forty_thou, 'cryogen')

        match_gen = mw.substring('t', 'i', 'e')
        self.assertEqual(next(match_gen), 'abelite')

        t = mw.trie()
        applesa = t['a']['p']['p']['l']['e']['s']['a']
        self.assertEqual(applesa, {'u': {'c': {'e': {'_': None}}}})
        zoolo = t['z']['o']['o']['l']['o']
        self.assertEqual(list(zoolo.keys()), ['g'])

    # sorted files
    def test_sorted_files(self):
        test_dir = 'test_directory'
        fst = [test_dir, 'images', 'hofstadter_xkcd.png']
        snd = [test_dir, 'alice.txt']
        thrd = [test_dir, 'images', 'xkcd_kepler.png']
        ans = [os.path.join(*x) for x in (fst, snd, thrd)]
        self.assertEqual(sorted_files(test_dir), ans)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
